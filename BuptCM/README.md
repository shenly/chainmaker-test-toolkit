# 1、准备环境

- JDK>=1.8（推荐1.8版本）

* MySQL>=5.7.0（推荐5.7版本）
* Redis>=3.0
* Maven>=3.0
* Node>=12
* Nacos>=1.1.0（ruoyi-cloud>=3.0.0需下载Nacos>=2.x.x版本）
* Sentinel>=1.6.0

* nginx

​    注意：推荐使用nacos的稳定版本——nacos2.0.3





# 2、nginx 安装部署

（1）下载地址

​    地址http://nginx.org/en/download.html, 如下载的版本为1.20.2

![](部署流程说明.assets/nginx.jpg)

下载到本地电脑，后上传至Linux服务器。

（2）解压安装包

```sh
tar xzf nginx-1.20.2.tar.gz   #解压文件夹，版本为下载的具体版本
mv nginx-1.20.2 /user/local/nginx    #移动目录
```

（3）安装nginx

```sh
cd /user/local/nginx   #进入nginx目录
./configure --prefix=/usr/local/nginx --conf-path=/usr/local/nginx/nginx.conf    #检测安装平台的目标特征
```

常见会缺少gcc、zlib、PCRE依赖，可使用下面三个命令安装依赖

```sh
yum install -y gcc;
yum install -y pcre pcre-devel;
yum install -y zlib zlib-devel
```

（4）编译nginx

configure命令成功之后，输入命令`make`，编译nginx目录；make成功，最后输入命令`make install /usr/local/nginx/`安装nginx

（5）启动nginx

```sh
cd /usr/local/nginx/sbin/                           #进入nginx的sbin目录
./nginx -c /usr/local/nginx/conf/nginx.conf         #启动命令
```

（6）复制dist包到指定目录下：[nginx下载位置]/html

（7）修改nginx配置  

```sh
vim nginx.conf                           #进入/usr/local/nginx/conf
```

修改端口号、root指向dist文件夹、try_files $uri $uri/ /index.html->加上这句话，刷新不会报404

```sh
./nginx -s reload                            #保存配置文件后，重启
```

打开浏览器，输入服务器上对应的ip地址和端口即可看见页面。  





# 3、MySQL安装部署

（1）运行MySQL部署脚本 mysql_install.sh  

```sh
sudo bash mysql_install.sh
```





# 4、Redis安装部署

（1）运行Redis部署脚本redis_install.sh

```sh
sudo bash redis_install.sh
```

（2）查看Redis服务状态

```sh
service redis status
```

若提示: 

```sh
redisd.service

  Loaded: not-found (Reason: No such file or directory)

  Active: inactive (dead)
```

**解决办法**：

- 1)新建文件redisd.service，

```sh
vim redisd.service
```

​	redisd.service内容：

```sh
[Unit]

Description=Redis Server

After=network.target

 
[Service]

ExecStart=/usr/local/bin/redis-server /etc/redis/redis.conf

ExecStop=/usr/local/bin/redis-cli shutdown

User=redis

Group=redis

Restart=always


[Install]

WantedBy=multi-user.target
```

- 2）将该文件保存到 /etc/systemd/system 目录下

```sh
sudo nano /etc/systemd/system/redisd.service
```

- 3）重新执行脚本

```sh
sudo bash redis_install.sh
```





# 5、nacos安装部署

（1）运行Nacos2.0.3部署脚本 nacos_install.sh

```sh
sudo bash nacos_install.sh
```

（2）创建映射容器的文件目录

```sh
mkdir -p /mydata/nacos/logs/      #创建logs目录
mkdir -p /mydata/nacos/conf/      #创建配置文件目录
# 授予权限
chmod 777 /mydata/nacos/logs
chmod 777 /mydata/nacos/conf
```

（3）创建nacos_config数据库

  Nacos使用2.0.3版本后，创建的数据库由”nacos“变为”nacos_config“，需要手动创建数据库，进入mysql使用如下SQL语句：

```mysql
CREATE DATABASE `nacos_config` DEFAULT CHARACTER SET utf8mb4;
# 这里的数据库名称使用了关键字config，所以要使用``对数据库名称进行包围，后面在创建ry-config等数据库时也需要同样的操作，否则无法创建成功。
```

（4）执行SQL语句，为nacos_config添加数据表

 		 SQL文件位置：[nacos下载位置]/nacos/conf/nacos-mysql.sql

​		  进入mysql后，使用如下命令执行SQL文件：

```mysql
mysql> source [下载位置]/nacos/conf/nacos-mysql.sql
```

（5）修改application.properties文件将nacos与mysql数据库进行连接，具体修改内容如下：

```properties
# 取消下面内容的注释
 spring.datasource.platform=mysql
 db.num=1
 db.url.0=jdbc:mysql://localhost:3306/nacos_config?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true&useUnicode=true&useSSL=false&serverTimezone=UTC
 db.user=自己的用户名
 db.password=自己的密码
```

（6）命令行启动Nacos

​    运行/nacos/bin/startup.sh脚本启动服务

​    使用如下命令在单体状态下启动Nacos

```sh
# 适用于centos
sh startup.sh -m standalone
# 适用于Ubantu
bash startup.sh -m standalone
# 不同的操作系统执行脚本的命令不相同，需要注意区分
```





# 6、数据库与配置文件修改

（1）创建相应数据库

- 创建数据库ry-cloud并导入数据脚本ry_2023xxxx.sql（必须），quartz.sql（可选），ry-cloud.sql（必须）；

- 创建数据库ry-config并导入数据脚本ry_config.sql（必须）；

   为保证编码正确，建议在sql文件中添加如下语句，按照指定编码自动创建相应数据库。

  

*创建ry-cloud数据库（将本部分加在对应SQL文件的前端）*

```mysql
 DROP DATABASE IF EXISTS `ry-cloud`;
 CREATE DATABASE `ry-cloud` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
 SET NAMES utf8mb4;
 USE `ry-cloud`;
```

*创建ry-config数据库*

```mysql
DROP DATABASE IF EXISTS `ry-config`;
 CREATE DATABASE `ry-config` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
 SET NAMES utf8mb4;
 USE `ry-config`;
```

（2）修改nacos配置文件

 	修改application.properties文件，修改文件中有关数据库的配置，将ruoyi所需要的配置文件信息加载到nacos中，具体修改内容如下：

```properties
# 取消下面内容的注释
 spring.datasource.platform=mysql
 db.num=1
 db.url.0=jdbc:mysql://localhost:3306/ry-config?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true&useUnicode=true&useSSL=false&serverTimezone=UTC
 db.user=自己的用户名
 db.password=自己的密码
```

（3）输入网址导入相关配置

​    地址栏中输入：[nacos所在服务器ip]:8848/nacos，导入yml文件夹中相应配置。

![](部署流程说明.assets/nacos.jpg)

（4）修改配置中有关端口号、IP地址信息 

![](部署流程说明.assets/port.jpg)

```
# 如上图所示，修改以下配置的3306端口号
 buptCM-chainmaker-dev.yml
 buptCM-gen-dev.yml
 buptCM-job-dev.yml
 buptCM-practice-dev.yml
 buptCM-smartcontract-dev.yml
 buptCM-stresstest-dev.yml
 buptCM-system-dev.yml
```

```
# 修改以下配置的IP
 buptCM-auth-dev.yml
 buptCM-chainmaker-dev.yml
 buptCM-file-dev.yml
 buptCM-gateway-dev.yml
 buptCM-gen-dev.yml
 buptCM-job-dev.yml
 buptCM-practice-dev.yml
 buptCM-smartcontract-dev.yml
 buptCM-stresstest-dev.yml
 buptCM-system-dev.yml
 buptCM-gateway-dev.yml
```

 

 