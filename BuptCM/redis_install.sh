#!/bin/bash

apt-get update
apt-get install -y build-essential tcl wget

REDIS_VERSION="3.0.5"
REDIS_DOWNLOAD_URL="http://download.redis.io/releases/redis-${REDIS_VERSION}.tar.gz"

wget ${REDIS_DOWNLOAD_URL}
if [ -f redis-${REDIS_VERSION}.tar.gz ]; then
    tar zxvf redis-${REDIS_VERSION}.tar.gz
    mv redis-${REDIS_VERSION} /usr/local/redis
    cd /usr/local/redis
    make
    cd src/
    make install
else
    echo "文件不存在!"
    exit
fi

sed -i 's/daemonize no/daemonize yes/' /usr/local/redis/redis.conf
sed -i 's/bind 127.0.0.1/bind 0.0.0.0/' /usr/local/redis/redis.conf
sed -i 's/protected-mode yes/protected-mode no/' /usr/local/redis/redis.conf

mkdir -p /etc/redis
ln -s /usr/local/redis/redis.conf /etc/redis/6379.conf

wget https://raw.githubusercontent.com/antirez/redis/4.0/redis.conf -O /etc/redis/redis.conf

ln -s /usr/local/redis/utils/redis_init_script /etc/init.d/redisd
service redisd start

#redis-cli
netstat -ntpl | grep redis

echo "Redis 部署完成！"
echo " "
echo "如果你的系统是 Ubuntu 18.04/20.04，在安装完毕后留意防火墙，可执行以下命令来放行 Redis 外部通信。"
echo "ufw allow 6379"
